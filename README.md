# Guida al PC

Questa guida introduce i concetti basilari per l'utilizzo dei PC forniti da ["Trashware Cesena"](http://trashwarecesena.it/) e spunti per l'approfondimento attraverso le risorse già presenti in rete.

**La guida è in pieno sviluppo, quindi la versione non è definitiva.**
I template latex è [tufte style book](http://www.latextemplates.com/template/tufte-style-book).
